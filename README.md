# Open IoT SDK atomics

## Overview

This repository provides a library of cross-platform atomic memory access functions analogous to the respective C11 and C++11 standard library facilities.

* Loads have acquire semantics,
* stores have release semantics,
* atomic operations are sequentially consistent,
* atomicity is enforced both between threads and interrupt handlers.

This library is a dependency to some of the Open IoT SDK components, but can be also used independently.

For details about the available functions and their use see the Doxygen documentation.

## License and contributions

The software is provided under the Apache-2.0 license. All contributions to software and documents are licensed by contributors under the same license model as the software/document itself (ie. inbound == outbound licensing). Open IoT SDK may reuse software already licensed under another license, provided the license is permissive in nature and compatible with Apache v2.0.

Folders containing files under different permissive license than Apache 2.0 are listed in the LICENSE file.

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## Security issues reporting

If you find any security vulnerabilities, please do not report it in the GitLab issue tracker. Instead, send an email to the security team at arm-security@arm.com stating that you may have found a security vulnerability in the Open IoT SDK.

More details can be found at [Arm Developer website](https://developer.arm.com/support/arm-security-updates/report-security-vulnerabilities).
