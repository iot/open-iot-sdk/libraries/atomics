# v2023.04 (2023-04-13)

## Changes

* doc: Update README.md with project information
* ci: Add .gitlab-ci.yml to enable pipelines.


This changelog should be read in conjunction with release notes provided
for a specific release version.
