/*
 * Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef IOTSDK_PRIVATE_TOOLCHAIN_H
#define IOTSDK_PRIVATE_TOOLCHAIN_H

#ifndef IOTSDK_UNUSED
#if defined(__GNUC__) || defined(__clang__)
#define IOTSDK_UNUSED __attribute__((__unused__))
#else
#define IOTSDK_UNUSED
#endif
#endif

#ifndef IOTSDK_FORCEINLINE
#if defined(__GNUC__) || defined(__clang__)
#define IOTSDK_FORCEINLINE inline __attribute__((always_inline))
#elif defined(__ICCARM__)
#define IOTSDK_FORCEINLINE _Pragma("inline=forced")
#else
#define IOTSDK_FORCEINLINE inline
#endif
#endif

#if defined(__GNUC__) || defined(__clang__) || defined(__ICCARM__)
#define IOTSDK_COMPILER_BARRIER() asm volatile("" : : : "memory")
#else
#error "Missing IOTSDK_COMPILER_BARRIER implementation"
#endif

#define IOTSDK_BARRIER() IOTSDK_COMPILER_BARRIER()

#endif // IOTSDK_PRIVATE_TOOLCHAIN_H
